<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 16.11.18
 * Time: 6:42
 */

namespace app\models\oop;

use app\models\oop\models\Accuracy;
use app\models\oop\models\Price;
use app\models\oop\models\Tolerance;

class PriceControl
{
  const DEFAULT_VALUE_ACCURACY = 2;
  const PERCENT_KEY = 100;
  /** @var Price $current_price */
  public $current_price;
  /** @var Price $previous_price */
  public $previous_price;
  /** @var Tolerance $tolerance */
  public $tolerance;
  /** @var Accuracy $accuracy */
  public $accuracy = self::DEFAULT_VALUE_ACCURACY;
  /** @var integer $result*/
  private $result;

  public function __construct(Price $current, Price $previous, Tolerance $tolerance = null)
  {
    $this->setCurrentPrice($current);
    $this->setPreviousPrice($previous);
    $this->setTolerance($tolerance);
  }

  /**
   * Производит проверку текущей цены в зависимости от прошлой
   */
  public function checkPrice()
  {
    $difference = $this->getCurrentPrice()->getAmount() * self::PERCENT_KEY / $this->getPreviousPrice()->getAmount();
    $result = round($difference, self::DEFAULT_VALUE_ACCURACY);
    $this->setResult($result);
  }

  /**
   * Возвращает маркер отклонения
   * @return bool
   */
  public function diff()
  {
    return (bool) ($this->getResult() <= $this->getTolerance()->getAmount());
  }

  /**
   * @return Price
   */
  public function getCurrentPrice()
  {
    return $this->current_price;
  }

  /**
   * @param Price $current_price
   * @return PriceControl $this;
   */
  public function setCurrentPrice($current_price)
  {
    $this->current_price = $current_price;
    return $this;
  }

  /**
   * @return Price
   */
  public function getPreviousPrice()
  {
    return $this->previous_price;
  }

  /**
   * @param Price $previous_price
   * @return PriceControl $this;
   */
  public function setPreviousPrice($previous_price)
  {
    $this->previous_price = $previous_price;
    return $this;
  }

  /**
   * @return Tolerance
   */
  public function getTolerance()
  {
    return $this->tolerance;
  }

  /**
   * @param Tolerance $tolerance
   * @return PriceControl $this;
   */
  public function setTolerance($tolerance)
  {
    $this->tolerance = $tolerance;
    return $this;
  }

  /**
   * @return Accuracy
   */
  public function getAccuracy()
  {
    return $this->accuracy;
  }

  /**
   * @param Accuracy $accuracy
   * @return PriceControl $this;
   */
  public function setAccuracy($accuracy)
  {
    $this->accuracy = $accuracy;
    return $this;
  }

  /**
   * @return int
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * @param int $result
   * @return PriceControl $this;
   */
  public function setResult($result)
  {
    $this->result = $result;
    return $this;
  }

}