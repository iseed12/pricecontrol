<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 16.11.18
 * Time: 8:46
 */

namespace app\models\oop\models;


use yii\base\Model;
use app\models\oop\exceptions\ValidException;


class Price extends Model
{
  public $amount;

  public function rules()
  {
    return [
      ['amount',
       'match',
       'pattern' => '/^[0-9]+(\.[0-9]{1,2})*$/',
       'message' => 'Цена указана неверно'
      ],
    ];
  }

  public function check()
  {
    $this->validate();
    if($this->hasErrors())
      throw new ValidException(array_shift($this->getErrors('amount')));
    return $this;
  }

  /**
   * @return mixed
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * @param mixed $amount
   * @return Price $this;
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;
    return $this;
  }


}