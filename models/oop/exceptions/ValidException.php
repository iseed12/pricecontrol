<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 16.11.18
 * Time: 8:38
 */

namespace app\models\oop\exceptions;

class ValidException extends \Exception
{

  private $errors = [];

  public function __construct($message = "", $errors = [], $code = 0)
  {
    parent::__construct($message, $code);
    $this->setErrors($errors);
  }

  /**
   * @return array
   */
  public function getErrors()
  {
    return $this->errors;
  }

  /**
   * @param array $errors
   * @return ValidException $this;
   */
  public function setErrors($errors)
  {
    $this->errors = $errors;
    return $this;
  }

}