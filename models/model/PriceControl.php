<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 16.11.18
 * Time: 6:42
 */

namespace app\models\model;

use yii\base\Model;

class PriceControl extends Model
{
  const DEFAULT_VALUE_ACCURACY = 2;
  const PERCENT_KEY = 100;
  public $current_price;
  public $previous_price;
  public $tolerance;
  public $accuracy = self::DEFAULT_VALUE_ACCURACY;
  private $result;

  public function rules()
  {
   return [
     [['current_price', 'previous_price'],
      'required',
      'message' => 'Обязательный параметр не указан'
     ],
     [['current_price', 'previous_price'],
      'match',
      'pattern' => '/^[0-9]+(\.[0-9]{1,2})*$/',
      'message' => 'Обязательный параметр указан неверно'
     ],
     ['tolerance', 'integer', 'message' => 'Допустимое значение указано неверно']
   ];
  }

  public function __construct($params)
  {
    parent::__construct($params);
  }

  /**
   * Производит проверку текущей цены в зависимости от прошлой
   * @throws ValidException
   */
  public function checkPrice()
  {
    $this->validate();
    if($this->hasErrors())
      throw new ValidException('При проверке цены обнаружены ошибки', $this->getErrors());
    $difference = $this->getCurrentPrice() * self::PERCENT_KEY / $this->getPreviousPrice();
    $result = round($difference, self::DEFAULT_VALUE_ACCURACY);
    $this->setResult($result);
  }

  /**
   * Возвращает маркер отклонения
   * @return bool
   */
  public function diff()
  {
    return (bool) ($this->getResult() <= $this->getTolerance());
  }

  /**
   * @return mixed
   */
  public function getCurrentPrice()
  {
    return $this->current_price;
  }

  /**
   * @param mixed $current_price
   * @return PriceControl $this;
   */
  public function setCurrentPrice($current_price)
  {
    $this->current_price = $current_price;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getPreviousPrice()
  {
    return $this->previous_price;
  }

  /**
   * @param mixed $previous_price
   * @return PriceControl $this;
   */
  public function setPreviousPrice($previous_price)
  {
    $this->previous_price = $previous_price;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getTolerance()
  {
    return $this->tolerance;
  }

  /**
   * @param mixed $tolerance
   * @return PriceControl $this;
   */
  public function setTolerance($tolerance)
  {
    $this->tolerance = $tolerance;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * @param mixed $result
   * @return PriceControl $this;
   */
  public function setResult($result)
  {
    $this->result = $result;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getAccuracy()
  {
    return $this->accuracy;
  }

  /**
   * @param mixed $accuracy
   * @return PriceControl $this;
   */
  public function setAccuracy($accuracy)
  {
    $this->accuracy = $accuracy;
    return $this;
  }

}