<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 01.12.16
 * Time: 23:03
 */

namespace app\controllers;

use app\models\base\PriceControl as BasePriceControl;
use app\models\model\PriceControl as ModelPriceControl;
use app\models\model\ValidException as ModelValidException;
use app\models\oop\exceptions\ValidException as OopValidException;
use app\models\oop\models\Price;
use app\models\oop\models\Tolerance;
use app\models\oop\PriceControl as OopPriceControl;
use yii\web\Controller;

class HelpController extends Controller
{
    public function actionIndex()
    {
      $currentPrice = 10.95;
      $previousPrice = 8.40;
      $tolerancePercent = 140;
      $priceControl = new BasePriceControl($currentPrice, $previousPrice, $tolerancePercent);
      $priceControl->checkPrice();
      $isDiff = $priceControl->diff();
      $result = $priceControl->getResult();
      echo "base<br>";
      var_dump($isDiff);
      var_dump($result);
      echo "<br>";
      try{
        $params = [
          'current_price' => $currentPrice,
          'previous_price' => $previousPrice,
          'tolerance' => $tolerancePercent,
        ];
        $priceControl = new ModelPriceControl($params);
        $priceControl->checkPrice();
        $isDiff = $priceControl->diff();
        $result = $priceControl->getResult();
        echo "base<br>";
        var_dump($isDiff);
        var_dump($result);
        echo "<br>";
      } catch (ModelValidException $e){
        echo $e->getMessage(). "<br>";
        foreach ($e->getErrors() as $key => $error)
          foreach ($error as $errorMessage)
            echo $key . ' ' . $errorMessage . "<br>";
      }
      try{
        $current = (new Price())->setAmount($currentPrice)->check();
        $previous = (new Price())->setAmount($previousPrice)->check();
        $tolerance = (new Tolerance())->setAmount($tolerancePercent)->check();
        $priceControl = new OopPriceControl($current, $previous, $tolerance);
        $priceControl->checkPrice();
        $isDiff = $priceControl->diff();
        $result = $priceControl->getResult();
        echo "base<br>";
        var_dump($isDiff);
        var_dump($result);
        echo "<br>";
      } catch (OopValidException $e){
        echo $e->getMessage(). "<br>";
        foreach ($e->getErrors() as $key => $error)
          foreach ($error as $errorMessage)
            echo $key . ' ' . $errorMessage . "<br>";
      }

    }

}